#include <iostream>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
//IlhamKusumaAryanda

using namespace std;

int pil;
void pilih();


struct Tree{
      int data;
      Tree *kiri;
      Tree *kanan;
};

//Tree *nodeBaru, *daun, *daun1, *daun2, *daun3, *daun4;//

//fungsi untuk menambahkan node baru
void tambah(Tree **current, int databaru)
{
      //jika curreng masih kosong
      if((*current) == NULL)
      {
            //membuat node baru
            Tree *baru;
            //pengalokasian memori dari node yang telah dibuat
            baru = new Tree;
            //inisialisasi awal node yang baru dibuat
            baru->data = databaru;
            baru->kiri = NULL;
            baru->kanan = NULL;
            (*current) = baru;
            (*current)->kiri = NULL;
            (*current)->kanan = NULL;
            cout << "Data dimasukkan";
      }
     //jika data yang akan dimasukkan lebih kecil daripada current, maka akan diletakkan di node sebelah kiri.
      else if(databaru<(*current)->data)
            tambah(&(*current)->kiri, databaru);

     //jika data yang akan dimasukkan lebih besar daripada current, maka akan diletakkan di node sebelah kanan
      else if(databaru>(*current)->data)
            tambah(&(*current)->kanan, databaru);

     //jika saat dicek data yang akan dimasukkan memiliki nilai yang sama dengan data pada current
      else if(databaru == (*current)->data)
            cout << "Data sudah ada!";
}


void preOrder(Tree *current)
{
      if(current != NULL){
            cout << " " << current->data; //parent/root
            preOrder(current->kiri); //kiri
            preOrder(current->kanan); //kanan
      }
}

void inOrder(Tree *current)
{
      if(current != NULL){
            inOrder(current->kiri); //kiri
            cout << " " << current->data; //parent/root
            inOrder(current->kanan);   //kanan
      }
}


void postOrder(Tree *current)
{
      if(current != NULL){
            postOrder(current->kiri); //kiri
            postOrder(current->kanan); //kanan
            cout << " " << current->data; //parent/root
      }
}

//fungsi utama

int main()
{
      //deklarasikan variabel
      int pil, data;
      Tree *pohon;
      pohon = NULL;
      do
      {
            system("cls");
            cout << "\nTree C++";
            cout << "\nMENU";
            cout << "\n.....\n";
            cout << "1. Tambah kiri\n";
            cout << "2. Tambah kanan\n";
            cout << "3. pre-order\n";
            cout << "4. in-order\n";
            cout << "5. post-order\n";
            cout << "6. Keluar\n";
            cout << "Pilih : ";
            cin >> pil;
            switch (pil)
            {
            case 1 :
                  cout << "\nINPUT kiri : ";
                  cout << "\nData dimasukkan : ";
                  cin >> data;
                  //memanggil fungsi untuk menambah node yang berisi data pada tree
                  tambah(&pohon, data);
                  break;

            case 2 :
                  cout << "\nINPUT kanan : ";
                  cout << "\nData dimasukkan : ";
                  cin >> data;
                  //memanggil fungsi untuk menambah node yang berisi data pada tree
                  tambah(&pohon, data);
                  break;

            case 3 :
                  cout << "Pre-Order : ";
                  if(pohon!=NULL)
                       //memanggil fungsi untuk mencetak data secara preOrder
                        preOrder(pohon);
                  else
                        cout << "kosong!";
                  break;

            case 4 :
                  cout << "In-Order : ";
                  if(pohon!=NULL)
                       //memanggil fungsi untuk mencetak data secara inOrder
                        inOrder(pohon);
                  else
                        cout << "kosong!";
                  break;

            case 5 :
                  cout << "Post-Order : ";
                  if(pohon!=NULL)
                       //memanggil fungsi untuk mencetak data secara postOrder
                        postOrder(pohon);
                  else
                        cout << ("kosong!");
                  break;
            }
            _getch();
      }while(pil != 6); //akan diulang jika input tidak sama dengan 6
      return 0;
}

 /*   daun1 = new Tree();
    daun1->data = 1;
    daun1->kiri = NULL;
    daun1->kanan = NULL;

    daun2 = new Tree();
    daun2->data = 2;
    daun2->kiri = NULL;
    daun2->kanan = NULL;

    daun3 = new Tree();
    daun3->data = 3;
    daun3->kiri = NULL;
    daun3->kanan = NULL;

    daun4 = new Tree();
    daun4->data = 4;
    daun4->kiri = NULL;
    daun4->kanan = NULL;

    daun1->kiri = daun2;
    daun1->kanan = daun3;
    daun3->kanan = daun4;

    daun = createTree(5);
    daun->kiri = createTree(3);
    daun->kanan = createTree(9);
    daun->kiri->kiri = createTree(2);
    daun->kiri->kanan = createTree(4);
    daun->kanan->kiri = createTree(8);
    daun->kanan->kanan = createTree(12);

    cout << "Pre Order Traversal: " << endl;
    preOrder(daun);
    cout << "\nIn Order Traversal: " << endl;
    inOrder(daun);
    cout << "\nPost Order Traversal: " << endl;
    postOrder(daun); */
